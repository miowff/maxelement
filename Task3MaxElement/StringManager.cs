﻿using System.Globalization;
using System;

using System.Text.RegularExpressions;
namespace Task3MaxElement
{
    public class StringManager : IStringManager
    {
        public Result ProcessLinesAndGetRresult(string[] strings)
        {

            if (strings == null)
            {
                throw new ArgumentNullException(nameof(strings));
            }
            Result result = new Result();
            result.allStringsFromFile = strings;
            double maxSum = double.MinValue;

            for (int i = 0; i < result.allStringsFromFile.Length; i++)
            {
                if (IsThisStringDefected(result.allStringsFromFile[i]))
                {
                    result.defectedStrings.Add(i);
                    continue;
                }
                if (GetSumOfStringElements(strings[i]) > maxSum)
                {
                    maxSum = GetSumOfStringElements(strings[i]);
                    result.numberOfStringWithMaxElementsSum = i ;
                }
            }

            return result;

        }
        private bool IsThisStringDefected(string _valueOfString)
        {
            if (_valueOfString == null)
            {
                throw new ArgumentNullException(nameof(_valueOfString));
            }
            Regex regex = new Regex(@"[^\d\s,.+\s-]|,$|^[,.\s]|[,.\s]{2,}");
            if (regex.IsMatch(_valueOfString)||string.IsNullOrEmpty(_valueOfString))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private double GetSumOfStringElements(string _valueOfString)
        {
            if (_valueOfString == null)
            {
                throw new ArgumentNullException(nameof(_valueOfString));
            }
            NumberFormatInfo numberFormat = new NumberFormatInfo()
            {
                NumberDecimalSeparator = ".",
            };
            double sum = 0;
            string[] elements = _valueOfString.Split(',');
            foreach (var element in elements)
            {
                sum += Convert.ToDouble(element, numberFormat);
            }
            return sum;
        }
    }
}
