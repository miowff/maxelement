﻿using System.Collections.Generic;
using System.Text;
using System;
namespace Task3MaxElement
{
    public class Result
    {
        public string[] allStringsFromFile;
        public List<int> defectedStrings = new List<int>();
        public Nullable<int> numberOfStringWithMaxElementsSum { get; set; }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(Constants.StringsDescription);
            for (int i = 0; i < allStringsFromFile.Length; i++)
            {
                builder.AppendLine("#"+i+"\t"+allStringsFromFile[i]);
            }
            builder.Append(Constants.StringWintMaxSumDescroption);
            builder.Append(numberOfStringWithMaxElementsSum);
            string result = builder.ToString();
            return result;
        }
        public string GetDefectedStrings()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(Constants.DefectedStringsDescription);
            foreach (var element in defectedStrings)
            {
                builder.AppendLine(element.ToString());
            }
            string result = builder.ToString();
            return result;
        }
    }
}
