﻿
namespace Task3MaxElement
{
    public class Constants
    {
        public const string InputRequest = "Введите путь к файлу";
        public const string PathError = "Неудалось найти файл,путь указан некорректно!Повторите ввод";
        public const string StringsDescription = "Список строк:";
        public const string DefectedStringsDescription = "Номера поломанных строк:";
        public const string StringWintMaxSumDescroption = "Строка с наибольшей суммой елементов:";
        public const string NegativeStringParamError = "кол-во строк не может быт отрицаптельным:";
    }
}
