﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Task3MaxElement
{
    public class ContainerBuilder
    {
        public IServiceProvider Build()
        {
            var container = new ServiceCollection();
            container.AddSingleton<IStringManager, StringManager>();
            container.AddSingleton<IFileReader, FileReader>();
            container.AddSingleton<IProgramManager, ProgramManager>();
            container.AddSingleton<IConsoleCommunication, UserMessages>();
            return container.BuildServiceProvider();
        }
    }
}
