﻿using System;
using Microsoft.Extensions.DependencyInjection;
namespace Task3MaxElement
{
    class Program
    {
        public static readonly IServiceProvider Container = new ContainerBuilder().Build();
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            var programManager = Container.GetService<IProgramManager>();
            programManager.Start();
        }
    }
}
