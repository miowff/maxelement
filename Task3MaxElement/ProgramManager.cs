﻿using System.IO;
using System.Linq;
using System;
using System.Collections.Generic;
namespace Task3MaxElement
{
    public class ProgramManager : IProgramManager
    {
        private readonly IStringManager _stringManager;
        private readonly IConsoleCommunication _consoleCommunication;
        private readonly IFileReader _fileReader;
        public ProgramManager(IStringManager stringManager, IConsoleCommunication consoleCommunication, IFileReader fileReader)
        {
            this._consoleCommunication = consoleCommunication ?? throw new ArgumentNullException(nameof(consoleCommunication));
            this._stringManager = stringManager ?? throw new ArgumentNullException(nameof(stringManager));
            this._consoleCommunication = consoleCommunication ?? throw new ArgumentNullException(nameof(consoleCommunication)); ;
            this._fileReader = fileReader ?? throw new ArgumentNullException(nameof(fileReader)); ;
        }

        public void Start()
        {
            string[] lines = _fileReader.GetAllLines(GetPath());
            Result result = _stringManager.ProcessLinesAndGetRresult(lines);
            _consoleCommunication.PrintMessage(result.GetDefectedStrings());
            _consoleCommunication.PrintMessage(result.ToString());

        }
        private string GetPath()
        {
            List<string> commandLineArgs = Environment.GetCommandLineArgs().ToList();
            if (commandLineArgs.Count<=1)
            {
                _consoleCommunication.PrintMessage(Constants.InputRequest);
                string path = _consoleCommunication.GetInput();
                while (!File.Exists(path))
                {
                    _consoleCommunication.PrintMessage(Constants.PathError);
                    path = _consoleCommunication.GetInput();
                }
                return path;
            }
            else
            {
                return commandLineArgs[1];
            }
        }

    }
}
