﻿using System.Collections.Generic;
namespace Task3MaxElement
{
    public interface IFileReader
    {
        public string[] GetAllLines(string path);
        
    }
}
