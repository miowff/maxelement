﻿using System;
namespace Task3MaxElement
{
    public interface IConsoleCommunication
    {
        public string GetInput();
        public void PrintMessage(object message);
    }
}
