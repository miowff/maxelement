﻿using System.IO;
using System;
namespace Task3MaxElement
{
    public class FileReader : IFileReader
    {
        public string[] GetAllLines(string path)

        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            string[] lines;
            lines = File.ReadAllLines(path);
            return lines;
        }

    }
}
