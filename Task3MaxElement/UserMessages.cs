﻿using System;
namespace Task3MaxElement
{
    public class UserMessages : IConsoleCommunication
    {
        private string _message { get; set; }
        public string GetInput()
        {
            this._message = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(this._message))
            {
                PrintMessage(Constants.PathError);
                this._message = Console.ReadLine();
            }
            return this._message;
        }
        public void PrintMessage(object message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }
            Console.WriteLine(message);
        }

    }
}
