using NUnit.Framework;
using Task3MaxElement;
using System.IO;
using System;
namespace ConsoleCommuinicationTests
{
    public class ConsoleCommuinicationTests
    {
        private UserMessages _consoleCommunication;
        [SetUp]
        public void Setup()
        {
            _consoleCommunication = new UserMessages();
        }

        [Test]
        public void UserInputTest()
        {
            //Arrange
            string expected = "3";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            var actual = _consoleCommunication.GetInput();
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void PrintMessageTest()
        {
            //Arrange
            var output = new StringWriter();
            Console.SetOut(output);
            //Act
            _consoleCommunication.PrintMessage("message");
            //Assert
            Assert.That(output.ToString(), Is.EqualTo(string.Format("message\r\n", Environment.NewLine)));
        }
        [Test]
        public void NullMessageTest()
        {
            // Arrange
            string expected =null;
            //Assert
            Assert.Throws<ArgumentNullException>(() => _consoleCommunication.PrintMessage(expected));
        }

    }
}