﻿using NUnit.Framework;
using Task3MaxElement;
using Microsoft.Extensions.DependencyInjection;
using System;
namespace ConsoleCommuinicationTests
{
    public class ContainerBuilderTests
    {
        public static readonly IServiceProvider Container = new ContainerBuilder().Build();
        [Test]
        public void ContainerBuilderIsNotNull()
        {
            //Assert
            Assert.NotNull(Container);
        }
        [Test]
        public void IsProvaiderContainsProgramManager()
        {
            //Arrange
            var programManager = Container.GetService<IProgramManager>();
            //Assert
            Assert.NotNull(programManager);
        }
        [Test]
        public void IsProviderContainsConsoleCommunication()
        {
            //Arrange
            var consoleCommunication = Container.GetService<IConsoleCommunication>();
            //Assert
            Assert.NotNull(consoleCommunication);
        }
        [Test]
        public void IsProviderContainsStringManager()
        {
            //Arrange
            var stringManager = Container.GetService<IStringManager>();
            //Assert
            Assert.NotNull(stringManager);
        }
        [Test]
        public void IsProviderContainsIFileReader()
        {
            //Arrange
            var fileReader = Container.GetService<IFileReader>();
            //Assert
            Assert.NotNull(fileReader);
        }
    }
}