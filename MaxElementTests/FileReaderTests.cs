﻿using NUnit.Framework;
using Task3MaxElement;
using System;
using System.IO;
namespace ConsoleCommuinicationTests
{
    public class FileRederTests
    {
        private FileReader _fileReader=new FileReader();
        [Test]
        public void CorrectPathInput()
        {
            //Arrange
            string[] lines = { "-10,-2.5,-15", "a,s,d", "a,s,d" };
            string path = Path.Combine(Environment.CurrentDirectory, "StringsFileTest.txt");
            var myFile = File.Create(path);
            myFile.Close();
            //Act
            File.AppendAllLines(path, lines);
            //Assert
            Assert.AreEqual(lines.Length, File.ReadAllLines(path).Length);
            File.Delete(path);
        }
        [Test]
        public void IncorrectPathInput()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => _fileReader.GetAllLines(@""));
        }

    }
}