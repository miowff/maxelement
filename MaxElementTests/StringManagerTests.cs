﻿using NUnit.Framework;
using Task3MaxElement;

namespace StringManagerTests
{
    public class StringManagerTests
    {
        StringManager stringManager = new StringManager();
       
        [Test]
        public void CountOfStringsTest()
        {
            //Arrange
            string[] strings = { "1,2,3", "5,2,1", "a,s,d" };
            //Act
            Result result = stringManager.ProcessLinesAndGetRresult(strings);
            //Assert

            Assert.AreEqual(result.allStringsFromFile.Length, strings.Length);
        }
        [Test]
        public void CountOfDefectedStringsTest()
        {
            //Arrange
            string[] strings = { "1,2,3", "5,2,1", "a,s,d" };
            //Act
            Result result = stringManager.ProcessLinesAndGetRresult(strings);
            //Assert
            Assert.AreEqual(result.defectedStrings.Count, 1);
        }
        [Test]
        public void MinSumTest()
        {
            //Arrange
            string[] strings = { "-10,-25,-15", "a,s,d", "a,s,d" };
            //Act
            Result result = stringManager.ProcessLinesAndGetRresult(strings);
            //Assert
            Assert.AreEqual(result.numberOfStringWithMaxElementsSum, 1);
        }
        [Test]
        public void AllStringsIsDEfectedTest()
        {
            //Arrange
            string[] strings = { "?*!@#$%^&", "?*!@#$%^&", "?*!@#$%^&" };
            //Act
            Result result = stringManager.ProcessLinesAndGetRresult(strings);
            //Assert
            Assert.AreEqual(result.numberOfStringWithMaxElementsSum, null);
        }
        [Test]
        public void IsIndexOfDefectedStringCorrect()
        {
            //Arrange
            string[] strings = { "1,2,3", "4,5,6", "a,s,d" };
            //Act
            Result result = stringManager.ProcessLinesAndGetRresult(strings);
            //Assert
            Assert.AreSame(result.defectedStrings[0], strings[2]);
        }
        public void DefectedАкactionalNumbersTest()
        {
            //Arrange
            string[] strings = { "..3,3,...3 ", "..,...,...,...,", "3, , , ,,,,,,,,,, ", "3,,,,,,,,,,,,,.......,", "3,3..3..3..3,3" };
            //Act
            Result result = stringManager.ProcessLinesAndGetRresult(strings);
            //Assert
            Assert.AreEqual(result.defectedStrings.Count,strings.Length);
        }
    }
}